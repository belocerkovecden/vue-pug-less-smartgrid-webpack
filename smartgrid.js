const smartgrid = require('smart-grid');
const settings = {
  outputStyle: 'less',
  columns: 12,
  offset: '15Px',
  mobileFirst: false,
  container: {
    maxWidth: '1400Px',
    fields: '15Px'
  },
  breakPoints: {
    lg: {
      width: '1280Px',
      fields: '15Px'
    },
    md: {
      width: '1100Px',
      fields: '15Px'
    },
    sm: {
      width: '768Px',
      fields: '10Px'
    },
    i5: {
      width: '320Px',
      offset: '3%',
      fields: '3%'
    }
  }
};

smartgrid('./src/styles/', settings);